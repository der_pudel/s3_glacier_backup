import base64

from s3_glacier_backup.glacier_client import ArchiveDesctiption


def test_archive_descriotion_enc_dec_plain():

    name = 'test_name'
    comment = 'test_comment'

    ad1 = ArchiveDesctiption(name, comment)

    json_cont = ad1.to_json()

    ad2 = ArchiveDesctiption.from_json(json_cont)

    assert ad2.name == name
    assert ad2.comment == comment

def test_archive_descriotion_enc_dec_encryption():

    name = 'test_name'
    comment = 'test_comment'
    encr  = 'test_encryption'
    vector  = b'test_vector'

    ad1 = ArchiveDesctiption(name, comment, encr, vector)

    json_cont = ad1.to_json()

    ad2 = ArchiveDesctiption.from_json(json_cont)

    assert ad2.name == name
    assert ad2.comment == comment
    assert ad2.encr == encr
    assert ad2.iv == vector