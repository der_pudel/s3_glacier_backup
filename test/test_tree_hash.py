import pytest

from s3_glacier_backup.tree_hash import TreeSha256


def test_tree_hash():

    test_data = b'1234567' * int((3.5 * 1024 ** 2) // 7)
    hash_ref = bytes.fromhex(
        'd59f30a748722585b28c31f5fcc2c44bd3b513d50bb00445999914e1e71bd262')

    tree_hash = TreeSha256()
    hash_u = tree_hash.update(test_data)
    hash_d = tree_hash.digest()

    assert hash_d == hash_u

    assert hash_ref == hash_d


def test_tree_hash_split_stream():

    test_data = b'1234567' * int((61 * 1024 ** 2) // 7)
    hash_ref = bytes.fromhex(
        '4674b8879bded86d8395e568c20bb17684c7e10c6a7b3f581d7c7044f2572b65')

    tree_hash = TreeSha256()

    block_size = 4 * 1024 ** 2

    while len(test_data) > 0:

        block = test_data[0: block_size]
        test_data = test_data[block_size:]

        tree_hash.update(block)

    hash_d = tree_hash.digest()

    assert hash_ref == hash_d


def test_exceptions():

    tree_hash = TreeSha256()

    with pytest.raises(TypeError):
        tree_hash.update(42)

    with pytest.raises(ValueError):
        tree_hash.update(b'')
