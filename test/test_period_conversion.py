import pytest

from datetime import timedelta
from s3_glacier_backup.utils import str_to_timedelta



def test_period_good_input():


    ref = timedelta(weeks=52 * 999)
    dat = str_to_timedelta("999Y")
    assert ref == dat

    ref = timedelta(weeks=4 * 42)
    dat = str_to_timedelta(" 42 M")
    assert ref == dat

    ref = timedelta(weeks=1)
    dat = str_to_timedelta("1W ")
    assert ref == dat

    ref = timedelta(days=5)
    dat = str_to_timedelta(" 5D")
    assert ref == dat


def test_period_bad_input():

    with pytest.raises(ValueError):
        str_to_timedelta("d")

    with pytest.raises(ValueError):
        str_to_timedelta("1")

    with pytest.raises(ValueError):
        str_to_timedelta("1a")

    with pytest.raises(ValueError):
        str_to_timedelta("1z")


    with pytest.raises(ValueError):
        str_to_timedelta("0d")

    with pytest.raises(ValueError):
        str_to_timedelta("-1d")