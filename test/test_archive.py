
import os
import random

from tempfile import TemporaryDirectory
from zipfile import ZipFile

import pytest


from s3_glacier_backup.archive import Archive


@pytest.fixture(scope="module")
def archive_data():

    min_size = 1 * 1024 ** 2
    max_size = 10 * 1024 ** 2

    rng_state = random.getstate()
    random.seed(0)

    with TemporaryDirectory(prefix="pytest-") as tmpdir:

        paths = []

        # directory with 2 files
        directory = os.path.join(tmpdir, "dir")
        paths.append(directory)
        os.mkdir(directory)

        for i in range(2):
            file = os.path.join(directory, f"file{i}.txt")
            size = random.randint(min_size, max_size)
            with open(file, "wb") as fh:
                fh.write(random.randbytes(size))

        # 1 stand-alone file
        file = os.path.join(tmpdir, "file.txt")
        paths.append(file)
        size = random.randint(min_size, max_size)
        with open(file, "wb") as fh:
            fh.write(random.randbytes(size))

        yield tmpdir, paths

    random.setstate(rng_state)


# contents of test_image.py
def test_archive_file(archive_data):  # pylint: disable=redefined-outer-name

    path, files = archive_data

    arch_name = "test_archive_file"
    arch = Archive(arch_name, files)
    arch.create_archive(path)

    arch_path = os.path.join(path, arch_name + ".zip")

    assert os.path.exists(arch_path)
    assert os.path.isfile(arch_path)

    with ZipFile(arch_path) as zip_file:
        assert zip_file.testzip() is None
