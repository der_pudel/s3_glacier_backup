import logging
import os
import zipfly


logger = logging.getLogger('archive')


class ArchiveStream():

    def __init__(self, name: str, gen, block_size: int) -> None:

        self._name = name
        self._gen = gen
        self._size = block_size

    @property
    def name(self):
        return self._name

    @property
    def generator(self):
        return self._gen

    @property
    def block_size(self):
        return self._size


class Archive:

    def __init__(self, name: str, source: list):

        self._name = name
        self._source = source

    @staticmethod
    def _fix_name(name: str):
        name, _ = os.path.splitext(name)
        name += ".zip"
        return name

    def _get_file_list(self, mixed_list: list):

        paths = []
        total_files = 0

        for path in mixed_list:
            path = os.path.abspath(os.path.expanduser(path))

            if os.path.isdir(path):
                for root, _, files in os.walk(path):
                    for file in files:
                        paths.append(os.path.join(root, file))
                    total_files += len(files)

            elif os.path.isfile(path):
                paths.append(path)
                total_files += 1
            else:
                logger.warning('Unrecognised path %s', path)

        return paths

    def _stream_enforce_chunk_size(self, generator, chunk: int):

        buffer = b''
        stream_end = False
        while True:
            if not stream_end:
                try:
                    buffer += next(generator)
                except StopIteration:
                    stream_end = True

            if len(buffer) >= chunk:
                ret = buffer[0:chunk]
                buffer = buffer[chunk:]
                yield ret
            elif stream_end:
                if len(buffer) > 0:
                    yield buffer
                break

    def create_archive_stream(self, chunk=128 * 1024 ** 2):

        # generate list of files
        paths = self._get_file_list(self._source)

        paths = [{"fs": p} for p in paths]

        zfly = zipfly.ZipFly(paths=paths, chunksize=chunk)
        zip_generator = zfly.generator()

        # Specifying chunk size for ZipFly library does not do anything usefull
        # actually. Chunks are inconsistent in size, but glacier multi-part
        # upload requires all chunks except last one to be the same size

        gen = self._stream_enforce_chunk_size(zip_generator, chunk)
        name = Archive._fix_name(self._name)

        return ArchiveStream(name, gen, chunk)

    def create_archive(self, dest: str):

        arch_stream = self.create_archive_stream()

        file = os.path.join(dest, arch_stream.name)

        with open(file, "wb") as fh:
            for block in arch_stream.generator:
                fh.write(block)
