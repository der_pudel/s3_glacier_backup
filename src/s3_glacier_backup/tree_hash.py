import hashlib


class TreeSha256():

    def __init__(self, block_size=1024 ** 2):

        self.block_size = block_size
        self.hashes = []

    def update(self, in_bytes):

        if not isinstance(in_bytes, bytes):
            raise TypeError("Input must be 'bytes' type")
        if len(in_bytes) == 0:
            raise ValueError("In bytes cannot be 0 length")

        hashes = []

        while len(in_bytes) > 0:

            block = in_bytes[0: self.block_size]
            in_bytes = in_bytes[self.block_size:]

            block_hash = hashlib.sha256()
            block_hash.update(block)
            block_hash = block_hash.digest()
            hashes.append(block_hash)

        result_hash = TreeSha256._tree_hash(hashes)

        # store the resutl of 1st round of tree hash calculation
        # intead of original block hashes to minimize mem usage
        self.hashes.append(result_hash)

        return result_hash

    def digest(self):
        return TreeSha256._tree_hash(self.hashes)

    @staticmethod
    def _tree_hash(hashes):

        while len(hashes) > 1:
            new_hashes = []
            for i in range(0, len(hashes), 2):
                if len(hashes) - i >= 2:
                    new_hash = hashlib.sha256()
                    new_hash.update(hashes[i])
                    new_hash.update(hashes[i+1])
                    new_hash = new_hash.digest()
                else:
                    new_hash = hashes[i]
                new_hashes.append(new_hash)
            hashes = new_hashes

        return hashes[0]
