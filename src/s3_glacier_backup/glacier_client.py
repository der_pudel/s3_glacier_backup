import logging
import json
import os
import math
import sqlite3
import base64

from datetime import datetime, timezone

from typing import List

import botocore
import boto3
from boto3_type_annotations.glacier.service_resource import Job, Vault

from Cryptodome.Protocol.KDF import scrypt
from Cryptodome.Cipher import AES

from s3_glacier_backup.tree_hash import TreeSha256
from s3_glacier_backup.archive import ArchiveStream

logger = logging.getLogger("glacier")


DB_PATH = "~/.glacier_backup/"
DB_NAME = "glacier_cache.db"


class ArchiveState():
    DELETED = 0
    ACTIVE = 1


class ArchiveDesctiption():

    def __init__(self, name: str, comment: str, encryption: str = None, iv: bytes = None):  # pylint: disable=invalid-name

        self.name = name
        self.comment = comment
        self.encr = encryption
        self.iv = iv  # pylint: disable=invalid-name

    def to_json(self):

        data = {
            "name": self.name,
            "comment": self.comment,
            "encr": self.encr,
            "iv": None if self.iv is None else base64.b64encode(self.iv).decode("ASCII"),
        }

        json_cont = json.dumps(data, sort_keys=True)
        if len(json_cont) > 1024:
            raise ValueError("Archive description is too long")
        return json_cont

    @staticmethod
    def from_json(json_cont: str):

        ad_dicr = json.loads(json_cont)

        init_vector = None
        if ad_dicr['iv'] is not None:
            init_vector = base64.b64decode(ad_dicr['iv'])

        return ArchiveDesctiption(
            name=ad_dicr['name'],
            comment=ad_dicr['comment'],
            encryption=ad_dicr['encr'],
            iv=init_vector,
        )


class GlacierClient():

    def __init__(self):
        self.client = boto3.client('glacier')
        self.resource = boto3.resource('glacier')

        self.account = '-'

        path = os.path.expanduser(DB_PATH)
        os.makedirs(path, exist_ok=True)
        path = os.path.join(path, DB_NAME)

        self.db_connection = sqlite3.connect(path)
        self.db_connection.execute("PRAGMA foreign_keys = 1")
        self.db_cursor = self.db_connection.cursor()

        self.default_chunk_size = 256 * 1024**2

        self._check_db_state()

    def close(self):
        # Save (commit) the changes
        self.db_connection.commit()

        # We can also close the connection if we are done with it.
        # Just be sure any changes have been committed or they will be lost.
        self.db_connection.close()

    def upload(self, file, vault, descr=""):

        if not os.path.exists(file):
            raise GlacierException(f"File does not exist: {file}")

        if not os.path.isfile(file):
            raise GlacierException(f"Is not a file {file}")

        self.create_vault(vault)

        size = os.path.getsize(file)

        filename = os.path.basename(file)

        with open(file, "rb") as fh:

            data = fh.read()
            file_hash = TreeSha256().update(data)

            response = self.client.upload_archive(accountId=self.account,
                                                  archiveDescription=filename,
                                                  body=data,
                                                  checksum=file_hash,
                                                  vaultName=vault)

            logger.debug("response: %s", str(response))
            archive_id = response['archiveId']

            date = datetime.now(timezone.utc).isoformat(timespec='seconds')

            self._insert_archive(archive_id=archive_id,
                                 name=file,
                                 descr=descr,
                                 vault=vault,
                                 size=size,
                                 archive_hash=file_hash,
                                 date=date)

        return

    def upload_multipart(self, archive_stream: ArchiveStream, vault: str, encrypt=False, comment=""):

        part_size = archive_stream.block_size
        filename = archive_stream.name

        if part_size < 1024 ** 2 or not math.log2(part_size).is_integer():
            raise GlacierException(
                "Part size must be multiple power of 2 and greather than 1 MB.")

        if part_size > 4 * 1024 ** 3:
            raise GlacierException("Part size must be smaller than 4GB")

        try:

            self.create_vault(vault)

            upload_id = None

            cipher = None

            if encrypt:

                key = self.get_encryption_key()
                if key is None:
                    raise GlacierException("Encryption is not configured")

                enc_type = "AES_CTR"
                cipher = AES.new(key, AES.MODE_CTR)

                init_vector = cipher.nonce
                archive_description = ArchiveDesctiption(filename, comment, enc_type, init_vector)
            else:
                archive_description = ArchiveDesctiption(filename, comment)
            description = archive_description.to_json()

            # Initiate

            response = self.client.initiate_multipart_upload(accountId=self.account,
                                                             partSize=f'{part_size}',
                                                             vaultName=vault,
                                                             archiveDescription=description)
            logger.debug("response: %s", str(response))

            upload_id = response['uploadId']

            # Upload

            start_byte = 0
            tree_hash = TreeSha256()
            for block in archive_stream.generator:

                if cipher is not None:
                    block = cipher.encrypt(block)

                part_hash = tree_hash.update(block).hex()

                end_byte = start_byte + len(block) - 1
                block_range = f'bytes {start_byte}-{end_byte}/*'
                start_byte += len(block)

                response = self.client.upload_multipart_part(accountId=self.account,
                                                             body=block,
                                                             checksum=part_hash,
                                                             range=block_range,
                                                             uploadId=upload_id,
                                                             vaultName=vault)
                logger.debug("response: %s", str(response))

            # finalize

            archive_size = start_byte
            upload_hash = tree_hash.digest().hex()
            # upload_hash = ""
            response = self.client.complete_multipart_upload(accountId=self.account,
                                                             archiveSize=f'{archive_size}',
                                                             checksum=upload_hash,
                                                             uploadId=upload_id,
                                                             vaultName=vault)
            logger.debug("response: %s", str(response))

            archive_id = response['archiveId']

            date = datetime.now(timezone.utc).isoformat(timespec='seconds')
            self._insert_archive(archive_id=archive_id,
                                 name=filename,
                                 descr=description,
                                 vault=vault,
                                 size=archive_size,
                                 archive_hash=upload_hash,
                                 date=date)

        except botocore.exceptions.ClientError:

            # Glacier.Client.exceptions.ResourceNotFoundException
            logger.exception("Multupart upload failed")
            if upload_id is not None:
                response = self.client.abort_multipart_upload(accountId=self.account,
                                                              uploadId=upload_id,
                                                              vaultName=vault)
                logger.debug("response: %s", str(response))

    def _download_archive(self, archive_retrieval_job: Job):

        if archive_retrieval_job.action != "ArchiveRetrieval" or not archive_retrieval_job.completed:
            raise GlacierException("Incorrect job")

        job_id = archive_retrieval_job.id
        archive_id = archive_retrieval_job.archive_id
        file_size = archive_retrieval_job.archive_size_in_bytes
        archive_hash_job = archive_retrieval_job.archive_sha256_tree_hash
        vault = archive_retrieval_job.vault_name

        if archive_retrieval_job.status_code == "Failed":
            reason = archive_retrieval_job.status_message
            if reason == "ARCHIVE_DELETED":
                logger.error("Job failed archive is deleted")
                self.delete_archive(vault, archive_id)
                return True
            else:
                logger.error("Job failed, reason %s", reason)
                return False


        param = (job_id,)
        self.db_cursor.execute("SELECT dest FROM pending_retrivals WHERE jobId=? ", param)
        row = self.db_cursor.fetchone()
        dest_dir = row[0]

        param = (archive_id,)
        self.db_cursor.execute("SELECT name, hash, descr FROM archives WHERE id=? ", param)

        row = self.db_cursor.fetchone()
        file_name = row[0]
        archive_hash_stored = row[1]
        descr_json = row[2]

        cipher = None
        if descr_json != "":
            description = ArchiveDesctiption.from_json(descr_json)

            if description.encr is None:
                pass
            elif description.encr == "AES_CTR":
                key = self.get_encryption_key()
                if key is None:
                    raise GlacierException("Key is not set, cannot retrieve encrypted archive")
                nonce = description.iv
                cipher = AES.new(key, AES.MODE_CTR, nonce=nonce)
            else:
                raise GlacierException(f"Unknown cipher '{description.encr}'", )

        if archive_hash_stored != archive_hash_job:
            raise GlacierException("Hash from the archive retrieval job does not match stored value")

        os.makedirs(dest_dir, exist_ok=True)

        out_file = os.path.join(dest_dir, file_name)
        out_file = os.path.expanduser(out_file)

        # https://stackoverflow.com/questions/27985599/downloading-a-large-archive-from-aws-glacier-using-boto

        tree_hash = TreeSha256()
        with open(out_file, 'wb') as fh:
            i = 0
            chunk_size = self.default_chunk_size
            more_chunks_available = True

            while more_chunks_available:

                start = 0 + chunk_size * i
                end = 0 + chunk_size * (i + 1) - 1

                if end >= file_size - 1:
                    end = file_size-1
                    more_chunks_available = False

                response = self.client.get_job_output(vaultName=vault,
                                                      jobId=job_id,
                                                      range=f'bytes={start}-{end}')
                logger.debug("response: %s", str(response))
                block = response['body'].read()
                tree_hash.update(block)

                if cipher is not None:
                    block = cipher.decrypt(block)

                fh.write(block)

                i += 1

            if tree_hash.digest().hex() != archive_hash_stored:
                raise GlacierException("Retrieved archie hash does not match")

        return True

    def _update_inventory(self, inventory_retrieval_job: Job):

        if inventory_retrieval_job.action != "InventoryRetrieval" or not inventory_retrieval_job.completed:
            raise GlacierException("Incorrect job")

        out = inventory_retrieval_job.get_output()
        resp = json.load(out['body'])

        active_id_list = []
        # insert missing archives
        for archive in resp["ArchiveList"]:

            active_id_list.append(archive['ArchiveId'])

            param = (archive['ArchiveId'],)
            self.db_cursor.execute(
                "SELECT id FROM archives WHERE id=? ", param)

            descr_json = archive['ArchiveDescription']
            try:
                archive_description = ArchiveDesctiption.from_json(descr_json)
                name = archive_description.name
            except json.decoder.JSONDecodeError:
                name = ""

            descr = archive['ArchiveDescription']

            if len(self.db_cursor.fetchall()) == 0:
                self._insert_archive(archive_id=archive['ArchiveId'],
                                     name=name,
                                     descr=descr,
                                     vault=inventory_retrieval_job.vault_name,
                                     size=archive['Size'],
                                     archive_hash=archive['SHA256TreeHash'],
                                     date=archive['CreationDate'])

        param = (ArchiveState.DELETED,) + tuple(active_id_list) + \
            (ArchiveState.ACTIVE, inventory_retrieval_job.vault_name)
        self.db_cursor.execute(f"""UPDATE archives
                                SET
                                    state = ?
                                WHERE
                                    NOT id IN ( {','.join('?' * len(active_id_list))} )
                                    AND
                                    state = ?
                                    AND
                                    vault = ?
                                    """, param)

        # mark all missing archives as deleted
        self.db_connection.commit()

        return True

    def _insert_archive(self, archive_id, name, descr, vault, size, archive_hash, date):
        param = (archive_id, name, vault, descr, size,
                 archive_hash, date, ArchiveState.ACTIVE)
        self.db_cursor.execute(
            'INSERT INTO archives VALUES (?,?,?,?,?,?,?,?)', param)
        self.db_connection.commit()

    def handle_job(self, job: Job):
        if not job.completed:
            return

        param = (job.id, job.action, job.completion_date)

        self.db_cursor.execute(
            "SELECT id FROM completed_jobs WHERE id=? AND type=? AND date=?", param)
        if len(self.db_cursor.fetchall()) != 0:
            return  # his job already porcessed

        handlers = {
            "InventoryRetrieval": self._update_inventory,
            "ArchiveRetrieval": self._download_archive,
        }
        handler = handlers.get(job.action)

        try:
            if handler is not None:
                processed = handler(job)
                if processed:
                    param = (job.id, job.action, job.completion_date)
                    self.db_cursor.execute(
                        'INSERT INTO completed_jobs VALUES (?,?,?)', param)
                    self.db_connection.commit()

        except Exception:  # pylint: disable=broad-except
            logger.exception("Job processing failed: %s", job.id)

    def retrieve_archive(self, vault: str, archive_id: str, dest: str) -> Job:

        # check if archive could be created

        param = (archive_id,)
        self.db_cursor.execute("SELECT name, hash FROM archives WHERE id=? ", param)

        row = self.db_cursor.fetchone()
        file_name = row[0]

        # check if file could be created before initiation the job to detect permission erros early
        dest = os.path.expanduser(dest)
        dest = os.path.abspath(dest)
        os.makedirs(dest, exist_ok=True)
        out_path = os.path.join(dest, file_name)
        fh = open(out_path, "wb")
        fh.close()
        os.remove(out_path)

        # start retrival job

        archive = self.resource.Archive(self.account, vault, archive_id)
        job = archive.initiate_archive_retrieval()

        param = (job.id, archive_id, dest)
        self.db_cursor.execute('INSERT INTO pending_retrivals VALUES (?,?,?)', param)

        self.db_connection.commit()

        return job

    def retrieve_invenotry(self, vault_name: str = None) -> Job:

        if vault_name is not None and vault_name not in [x.vault_name for x in self.list_vaults()]:
            raise GlacierException(f"vault {vault_name} does not exist")

        initiated = False
        for vault in self.list_vaults():
            if vault_name is None or vault.vault_name == vault_name:
                retrieve_allowed = True
                jobs = vault.jobs_in_progress.all()
                # prevent inventory retrieval if job is already pending
                for j in jobs:
                    if j.action != "InventoryRetrieval":
                        retrieve_allowed = False

                if retrieve_allowed:
                    vault.initiate_inventory_retrieval()
                    initiated = True

        return initiated

    def list_vaults(self) -> List[Vault]:

        vault_list = []

        param = {"accountId": self.account, "limit": '10'}

        while True:
            response = self.client.list_vaults(**param)
            logger.debug("response: %s", str(response))

            for vault in response['VaultList']:
                vault = self.resource.Vault(self.account, vault['VaultName'])
                vault_list.append(vault)
            if "Marker" in response:
                param["marker"] = response["Marker"]
            else:
                break

        return vault_list

    def list_archives(self, vault=None):

        if vault is None:
            param = (ArchiveState.ACTIVE,)
            conditions = "state=? "
        else:
            param = (ArchiveState.ACTIVE, vault)
            conditions = "state=?  AND vault=? "

        self.db_cursor.execute(
            f"SELECT * FROM archives WHERE {conditions} ORDER BY dateAdded ASC", param)

        result = []
        for row in self.db_cursor.fetchall():
            archive = {
                "id":   row[0],
                "name": row[1],
                "vault": row[2],
                "descr": row[3],
                "size": row[4],
                "hash": row[5],
                "date": row[6],
                "state": row[7],
            }
            result.append(archive)
        return result

    def delete_archive(self, vault: str, archive_id: str):

        param = {"accountId": self.account,
                 "archiveId": archive_id, "vaultName": vault}

        response = self.client.delete_archive(**param)
        logger.debug("response: %s", str(response))

        param = (ArchiveState.DELETED, archive_id, vault)

        self.db_cursor.execute("""UPDATE archives
                                SET
                                    state = ?
                                WHERE
                                    id = ? and vault = ? ;""", param)
        self.db_connection.commit()

    def delete_vault(self, vault: str):
        param = {"accountId": self.account, "vaultName": vault}

        try:
            response = self.client.delete_vault(**param)
            logger.debug("response: %s", str(response))
        except botocore.exceptions.ClientError:
            logger.exception("Vault deletion failed")

    def create_vault(self, vault: str):
        param = {"accountId": self.account, "vaultName": vault}

        try:
            response = self.client.create_vault(**param)
            logger.debug("response: %s", str(response))

        except botocore.exceptions.ClientError:
            logger.exception("Vault creation failed")

    # functions to get and set the time whn config was last updated
    def get_config_time(self, config_hash):

        param = (config_hash,)

        self.db_cursor.execute("SELECT date FROM configs WHERE hash=? ", param)
        row = self.db_cursor.fetchone()

        if row is None:
            return None
        else:
            return row[0]

    def set_config_time(self, config_hash, date=None):

        if date is None:
            date = datetime.now(timezone.utc)

        if isinstance(date, datetime):
            date = date.isoformat(timespec='seconds')
        else:
            raise GlacierException("Invalid date type.")

        param = (config_hash, date)
        self.db_cursor.execute("INSERT OR REPLACE INTO configs (hash, date) values(?, ?)", param)
        self.db_connection.commit()

    def set_encryption_key(self, password):

        salt = b'0123456789ABCDEF'
        key = scrypt(password, salt, 16, N=2**20, r=8, p=1)

        param = (0, key, )
        self.db_cursor.execute("INSERT OR REPLACE INTO encyption_key (id, key) values(?, ?)", param)
        self.db_connection.commit()

    def get_encryption_key(self):

        param = (0,)

        self.db_cursor.execute("SELECT key FROM encyption_key WHERE id=? ", param)
        row = self.db_cursor.fetchone()

        return row[0] if row is not None else None

    def _check_db_state(self):

        self.db_cursor.execute("""CREATE TABLE IF NOT EXISTS archives (
                                id TEXT NOT NULL PRIMARY KEY,
                                name TEXT NOT NULL,
                                vault TEXT NOT NULL,
                                descr TEXT NOT NULL,
                                size INTEGER NOT NULL,
                                hash TEXT NOT NULL,
                                dateAdded TEXT,
                                state INTEGER NOT NULL)""")

        self.db_cursor.execute("""CREATE TABLE IF NOT EXISTS completed_jobs (
                                id TEXT NOT NULL PRIMARY KEY,
                                type TEXT NOT NULL,
                                date TEXT NOT NULL)""")

        self.db_cursor.execute("""CREATE TABLE IF NOT EXISTS pending_retrivals (
                                jobId TEXT NOT NULL PRIMARY KEY,
                                archiveId TEXT NOT NULL,
                                dest TEXT NOT NULL,
                                FOREIGN KEY (archiveId) REFERENCES archives(id) )""")

        self.db_cursor.execute("""CREATE TABLE IF NOT EXISTS configs (
                                hash TEXT NOT NULL PRIMARY KEY,
                                date TEXT NOT NULL)""")

        self.db_cursor.execute("""CREATE TABLE IF NOT EXISTS encyption_key (
                                id INTEGER PRIMARY KEY CHECK (id = 0),
                                key BLOB NOT NULL)""")

        self.db_connection.commit()


class GlacierException(Exception):
    pass
