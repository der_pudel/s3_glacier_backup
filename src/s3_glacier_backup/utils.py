
from datetime import datetime, timedelta
import re
import hashlib
import json
import toml


def timestamp(time=None):

    if time is None:
        time = datetime.utcnow()

    return time.strftime("%Y-%m-%dT%H%M%S")

def str_to_timedelta(delta_str: str):

    delta_str1 = delta_str.lower().strip()

    match = re.match(r"^(\d+)([ymwd])$", delta_str1, re.IGNORECASE)
    if match is None:
        raise ValueError(f'Invalid time period "{delta_str}".')

    count, unit = match.groups()
    count = int(count)

    if unit == "d":
        days = count
    if unit == "w":
        days = count * 7
    if unit == "m":
        days = count * 7 * 4
    if unit == "y":
        days = count * 7 * 52

    if days <= 0:
        raise ValueError(f'Time period must be positive "{delta_str}".')


    return timedelta(days=days)


def dict_sha256(dictionary):

    sha256 = hashlib.sha256()
    encoded = json.dumps(dictionary, sort_keys=True).encode("utf-8")
    sha256.update(encoded)

    return sha256.hexdigest()


def config_load(conf_path):

    with open(conf_path, "r", encoding='utf-8') as fh:
        config = toml.load(fh)

    return config
