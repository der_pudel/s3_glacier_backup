#!/usr/bin/env python3

import logging
import logging.handlers
import os
import sys
import argparse
import shlex
from datetime import datetime, timezone, timedelta

from s3_glacier_backup.glacier_client import GlacierClient
from s3_glacier_backup.archive import Archive
from s3_glacier_backup.utils import config_load, timestamp, str_to_timedelta, dict_sha256


logger = logging.getLogger("main")


glacier = GlacierClient()


def logger_init(console_level=logging.DEBUG, file_level=None):

    formatter = logging.Formatter(
        '[%(asctime)s] %(filename)s:%(funcName)s %(levelname)s - %(message)s')

    handlers = []
    # create console handler
    if console_level is not None:
        console_handler = logging.StreamHandler()
        console_handler.setLevel(console_level)
        console_handler.setFormatter(formatter)
        console_handler.setFormatter(logging.Formatter('%(message)s'))
        handlers.append(console_handler)

    # create file handler which logs even debug messages
    if file_level is not None:
        file_handler = logging.handlers.RotatingFileHandler(
            sys.path[0] + '/log.txt', maxBytes=1024 ** 2, backupCount=1)
        file_handler.setLevel(file_level)
        file_handler.setFormatter(formatter)
        handlers.append(file_handler)

    logging.basicConfig(level=logging.DEBUG, handlers=handlers)


def logger_deinit():
    logging.shutdown()


#########################################################

class Cli:

    def __init__(self) -> None:

        self.interactive = False
        self.parser = self.create_arguments()

    @staticmethod
    def _parse_range(astr):
        result = set()
        for part in astr.split(','):
            range_sel = part.split('-')
            result.update(range(int(range_sel[0]), int(range_sel[-1])+1))
        return sorted(result)

    @staticmethod
    def _select_from_list(lst):
        for i, item in enumerate(lst):
            print(f"{i}. {item}")

        print()
        inp = input("Which ones? Enter numbers (empty to cancel): ")
        if inp == "":
            return None
        try:
            indexes = Cli._parse_range(inp)
            _ = lst[max(indexes)]
            _ = lst[min(indexes)]
            return indexes
        except (IndexError, ValueError) as ex:
            print("Error. " + str(ex))
            return None

    @staticmethod
    def _confirm_selection(items):

        print("Selected:")
        for i, item in enumerate(items):
            print(f"{i}. {item}")

        print()
        inp = input("Confirm (y/n)? ")
        if inp.lower() != "y":
            print("Canceled.")
            return False
        return True

    @staticmethod
    def _archive_list_to_str(iterable):
        return [f"{a['vault']}:{a['name']}\n\t{a['id']}" for a in iterable]

    @staticmethod
    def _select_path():

        result = None

        while True:
            inp = input("Where to download (empty to cance)? : ")
            if inp == "":
                break

            inp = os.path.expanduser(inp)
            try:
                os.makedirs(inp, exist_ok=True)
                result = inp
                break
            except PermissionError:
                print("Directory is not accessible")
        return result

    # List command

    # pylint: disable-next=unused-argument
    def cmd_list_valuts(self, args):
        vaults = glacier.list_vaults()
        if len(vaults) == 0:
            print("no vaults")
            return

        for i, vault in enumerate(vaults):
            print(f"{i}. {vault.name}\n\t{vault.vault_arn}")

    # pylint: disable-next=unused-argument
    def cmd_list_jobs(self, args):

        for vault in glacier.list_vaults():
            print(f'Vault "{vault.vault_name}"')
            no_jobs = True
            for i, job in enumerate(vault.jobs.all()):
                no_jobs = False
                print(
                    f"{i}. {job.action}, Started: {job.creation_date}, Completed: {job.completion_date}\n\t{job.id}")

                glacier.handle_job(job)

            if no_jobs:
                print("no active jobs")

    def cmd_list_archives(self, args):
        vault = args.vault

        archives = glacier.list_archives(vault)
        if len(archives) == 0:
            print("no archives")
        else:
            for i, archive in enumerate(archives):
                print(
                    f"{i}. {archive['vault']}:{archive['name']} {archive['descr']}\n\t{archive['id']}")

    # Retrieve command

    def cmd_retrieve_inv(self, args):
        vault = args.vault

        initiated = glacier.retrieve_invenotry(vault)
        print("initiated" if initiated else "Not initiated, check vault name.")

    def cmd_retrieve_arch(self, args):
        vault = args.vault
        archive_id = args.archive
        dest = args.dest

        archives = glacier.list_archives(vault)

        if len(archives) == 0:
            print("no archives")
            return

        to_retrieve = []
        if archive_id is None:
            archives_str = Cli._archive_list_to_str(archives)

            indexes = Cli._select_from_list(archives_str)

            if indexes is not None:
                confirm = [archives_str[i] for i in indexes]

                dest = Cli._select_path()
                if dest is None:
                    return

                if Cli._confirm_selection(confirm):
                    for i in indexes:
                        vault = archives[i]["vault"]
                        archive_id = archives[i]["id"]

                        to_retrieve.append((vault, archive_id, dest))
            else:
                return

        elif dest is None:
            dest = Cli._select_path()
            if dest is None:
                return
        else:
            to_retrieve.append((vault, archive_id, dest))

        # retrieve
        for vault, archive_id, dest in to_retrieve:

            if vault is None:
                for archive in archives:
                    if archive_id == archive['id']:
                        vault = archive['vault']
                        break

            glacier.retrieve_archive(vault, archive_id, dest)

    # Create command

    def cmd_create_vault(self, args):

        # max size = 255
        vault = args.vault

        glacier.create_vault(vault)

    # Delete command

    def cmd_delete_archive(self, args):
        vault = args.vault
        archive_id = args.id
        old = args.old
        assume_yes = args.yes

        archives = glacier.list_archives(vault)

        if len(archives) == 0:
            print("no archives")
            return

        if archive_id is not None:

            for archive in archives:
                if archive_id == archive['id']:
                    if vault is None:
                        vault = archive['vault']
                        break
                    else:
                        if vault != archive['vault']:
                            print("Vault does not match")
                            return
                else:
                    print("no archive with specified ID")

            glacier.delete_archive(vault, archive_id)

        if old is not None:

            now = datetime.now(timezone.utc)
            delta = str_to_timedelta(old)
            to_delete = []
            for archive in archives:
                date = datetime.fromisoformat(archive['date'])
                if (now - date) > delta:
                    to_delete.append(archive)

            if len(to_delete) < 1:
                print("no archives")
                return

            archives_str = Cli._archive_list_to_str(to_delete)
            if assume_yes or Cli._confirm_selection(archives_str):

                for archive in archives:
                    vault = archive['vault']
                    archive_id = archive['id']
                    glacier.delete_archive(vault, archive_id)

        else:   # interactive archive deletion

            archives_str = Cli._archive_list_to_str(archives)

            indexes = Cli._select_from_list(archives_str)

            if indexes is not None:
                confirm = [archives_str[i] for i in indexes]

                if Cli._confirm_selection(confirm):
                    for i in indexes:
                        vault = archives[i]["vault"]
                        archive_id = archives[i]["id"]
                        glacier.delete_archive(vault, archive_id)

    def cmd_delete_vault(self, args):

        vault = args.vault

        if vault is not None:
            glacier.delete_vault(vault)
            return

        vaults = glacier.list_vaults()

        if len(vaults) == 0:
            print("no vaults")
            return

        vaults_str = [f"{v.name}" for v in vaults]

        indexes = Cli._select_from_list(vaults_str)

        if indexes is not None:
            confirm = [vaults_str[i] for i in indexes]

            if Cli._confirm_selection(confirm):
                for i in indexes:
                    vault = vaults[i].name
                    glacier.delete_vault(vault)

    # Upload command

    def cmd_upload_config(self, args):

        config = args.config
        force = args.force

        config_backup = config_load(config)

        cfg_list = config_backup["backup"]

        now = datetime.now(timezone.utc)

        # offset to mitigate possible jitter in schduled launches
        freq_offset = timedelta(hours=1)

        for cfg in cfg_list:
            vault = cfg["vault"]
            arch_file = cfg["name"]
            file_list = cfg["backupDirs"]
            frequency = cfg["frequency"]
            encrypt = cfg["encrypt"]

            cfg_hash = dict_sha256(cfg)
            last_uploaded = glacier.get_config_time(cfg_hash)

            upload = False
            if last_uploaded is None:  # never uploaded
                upload = True
            elif force:
                upload = True
            else:
                freq_delta = str_to_timedelta(frequency)
                last_upload_date = datetime.fromisoformat(last_uploaded)
                if (now - last_upload_date) >= (freq_delta - freq_offset):
                    upload = True

            if upload:
                chunk = 2 * 1024 ** 2

                arch_file += "_" + timestamp()
                arch = Archive(arch_file, file_list)

                arch_stream = arch.create_archive_stream(chunk=chunk)

                glacier.upload_multipart(arch_stream, vault, encrypt)
                glacier.set_config_time(cfg_hash, now)

    # pylint: disable-next=unused-argument
    def cmd_encryption(self, args):

        if glacier.get_encryption_key() is not None:
            prompt = "password is already set, do you want to replace it? (y/n): "
            inp = input(prompt)
            if inp.lower() != 'y':
                print("Canceled")
                return


        print("Enter password: ")
        password = input()
        sys.stdout.write("\033[F\033[K") # Cursor up one lineand clear the line
        if len(password) < 8:
            print("Password must be at least 8 characters long.")
            return

        glacier.set_encryption_key(password)

        print(glacier.get_encryption_key())

    # Misc commands

    # pylint: disable-next=unused-argument
    def cmd_quit(self, args):
        self.interactive = False

    # pylint: disable-next=unused-argument
    def cmd_interactive(self, args):

        if self.interactive:
            print("already in interactive mode")
        else:

            self.interactive = True
            print("Interactive mode")
            while True:
                try:
                    line = input("> ")
                    argv = shlex.split(line)
                    self.handle_input(argv)

                except SystemExit:  # raised by help
                    pass
                except argparse.ArgumentError as ex:
                    print(ex)
                except Exception as ex:  # pylint: disable=broad-except
                    print("Command failed")
                    print(ex)

                if not self.interactive:
                    break

    def handle_input(self, argv):

        args = self.parser.parse_args(argv)
        args.command(args)

    def create_arguments(self):

        prog = os.path.basename(__file__)
        parser = argparse.ArgumentParser(prog=prog, description='Glacier backup interface',
                                         epilog=None, exit_on_error=False)

        subparsers = parser.add_subparsers(help='Commands', required=True)

        # Interactive
        parser_interactive = subparsers.add_parser('interactive',
                                                   help='start the application in interactive mode.')
        parser_interactive.set_defaults(command=self.cmd_interactive)

        # Quit
        parser_quit = subparsers.add_parser('quit',
                                            help='exit application (terminate interactive mode).')
        parser_quit.set_defaults(command=self.cmd_quit)

        # List command
        parser_list = subparsers.add_parser('list',
                                            help='set of commands for displaying archivrs, jobs, etc.')
        subparsers2 = parser_list.add_subparsers(required=True)

        parser_list_archives = subparsers2.add_parser('archives',
                                                      help='list of all archives')
        parser_list_archives.set_defaults(command=self.cmd_list_archives)
        parser_list_archives.add_argument("vault", action="store", nargs="?", default=None, type=str,
                                          help="vault from which archives will be listed (optional)")

        parser_list_vaults = subparsers2.add_parser('vaults',
                                                    help='list of all vaults')
        parser_list_vaults.set_defaults(command=self.cmd_list_valuts)

        parser_list_jobs = subparsers2.add_parser('jobs',
                                                  help='list of all active jobs')
        parser_list_jobs.set_defaults(command=self.cmd_list_jobs)

        # Retrieve
        parser_retrieve = subparsers.add_parser('retrieve',
                                                help='set of commands for retrieving archives and inventory.',
                                                epilog="Jobs take up to 24 hours to complete.")
        subparsers2 = parser_retrieve.add_subparsers(required=True)

        parser_list_inventory = subparsers2.add_parser('inventory',
                                                       help='initiate inventory retrieval job')
        parser_list_inventory.set_defaults(command=self.cmd_retrieve_inv)
        parser_list_inventory.add_argument("vault", action="store", nargs="?", default=None,
                                           type=str, help="vault name (optional)")

        parser_list_archive = subparsers2.add_parser('archive', help='initiate archive retrieval job',
                                                     epilog="If some og the optrions are not set, interactive \
                                                         mode will be started.")
        parser_list_archive.set_defaults(command=self.cmd_retrieve_arch)
        parser_list_archive.add_argument("vault", action="store", nargs="?", default=None, type=str,
                                         help="Vault name (optional)")
        parser_list_archive.add_argument("archive", action="store", nargs="?", default=None, type=str,
                                         help="archive id (optional)")
        parser_list_archive.add_argument("dest", action="store", nargs="?", default=None, type=str,
                                         help="directory where archive will be downloaded (optional)")

        # Create
        parser_create = subparsers.add_parser('create',
                                              help='set of commands for creating vaults')
        subparsers2 = parser_create.add_subparsers(help='TODO', required=True)

        parser_create_vault = subparsers2.add_parser('vault',
                                                     help='command for creating vaults')
        parser_create_vault.set_defaults(command=self.cmd_create_vault)
        parser_create_vault.add_argument("vault", action="store", default=None, type=str,
                                         help="vault name")

        # Delete
        parser_delete = subparsers.add_parser('delete',
                                              help='set of commands for deleteing vaults and artifacts')
        subparsers2 = parser_delete.add_subparsers(required=True)

        parser_delete_vault = subparsers2.add_parser('vault',
                                                     help='delete vault(must be empy)')
        parser_delete_vault.set_defaults(command=self.cmd_delete_vault)
        parser_delete_vault.add_argument("vault", action="store", default=None, type=str,
                                         help="vault name (optional)")

        parser_delete_archive = subparsers2.add_parser('archive',
                                                       help='delete archives from vault')
        parser_delete_archive.set_defaults(command=self.cmd_delete_archive)
        parser_delete_archive.add_argument("vault", action="store", nargs="?", default=None, type=str,
                                           help="vault name")
        parser_delete_archive.add_argument("-y", "--yes", action="store_true",
                                           help="automatic yes to confirmation prompts")

        group = parser_delete_archive.add_mutually_exclusive_group()

        group.add_argument("--id", action="store",  nargs="?", default=None, type=str,
                           help="dlete archive with specific ID")

        group.add_argument('--old', action="store",  nargs="?", default=None, type=str,
                           help="delete archives older than specified period, eg. '5d', '1w', '3m', '1y'")

        # Upload
        parser_upload = subparsers.add_parser('upload',
                                              help='command for uploading archives')
        parser_upload.set_defaults(command=self.cmd_upload_config)
        parser_upload.add_argument("--config", action="store", required=True, type=str,
                                   help="path to configuration TOML file")
        parser_upload.add_argument("-f", "--force",  action="store_true", required=False,
                                   help="force upload without checking period")

        # Encryption
        parser_encryption = subparsers.add_parser('encryption',
                                                  help='set encryption key')
        parser_encryption.set_defaults(command=self.cmd_encryption)

        return parser


def main():
    logger_init(logging.INFO, None)

    retcode = 1
    try:
        cli = Cli()
        cli.handle_input(sys.argv[1:])
        retcode = 0
    except Exception:  # pylint: disable=broad-except
        logger.exception("Unhandled exception")
    finally:
        glacier.close()

    logger_deinit()
    sys.exit(retcode)


if __name__ == '__main__':
    main()
