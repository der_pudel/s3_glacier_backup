#!/bin/bash

set -e

unset HISTFILE

unset LD_LIBRARY_PATH  # preffer local venv libraries to system ones
unset PYTHONPATH       # do not use system pip packages




#setup commands and paths for different OS
# https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script

PYTHON_VENV=python
PIP_VENV=pip
VENV_DIR=.venv
REQ_RUN="requirements.txt"


if [[ "$OSTYPE" == "msys" ||  "$OSTYPE" == "win32"  ||  "$OSTYPE" == "cygwin" ]]; then
# Windows
    PYTHON="py -3"
    ACTIVATE_VENV="$VENV_DIR/Scripts/activate"
else
# assume unix
    PYTHON="python3"
    ACTIVATE_VENV="$VENV_DIR/bin/activate"
fi



quiet_flag=""

if ! [ -d "$VENV_DIR" ]; then
    echo "creating new venv..."
    $PYTHON -m venv "$VENV_DIR"

    source "$ACTIVATE_VENV"

    $PYTHON_VENV -m pip install -U pip

    $PIP_VENV install wheel

    # install package in edit mode
    $PIP_VENV  install -e .

else
    source "$ACTIVATE_VENV"
    quiet_flag="-q"
fi


$PIP_VENV install -r "$REQ_RUN" $quiet_flag

if ! command -v aws --version &> /dev/null
then
    echo "AWS CLI not found."
    echo "https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html"
    exit
fi



DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

$PYTHON_VENV "$DIR/src/s3_glacier_backup/glacier_backup.py" "$@"
