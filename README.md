# Glacier S3 bachup interface

This program is a simple backup client that allows to archive  multiple directories on local file system and upload them to Amazon S3 Glacier.

## Prerequisites

* Install [AWS CLI](https://aws.amazon.com/cli/) and log in with the access key
* python 3.8+

## Usage


### Uploading

To backup files to S3 glacier, create tobl configuration file that specifies vault name, archive name and list of directories and or files. Config may contain more than 1 backup definition. Example:

```toml
# my_config.toml

[[backup]]
name = "test"
vault = "test_vault"
frequency = "1d"
encrypt = false
backupDirs = [ ".test_files/src",
               ".test_files/src2/yet_another_file.txt",
               "~/Pictures"  ]

[[backup]]
name = "very_important_files"
vault = "main_vault"
frequency = "1w"
encrypt = true
backupDirs = [
    #...
            ]
```

You cnan start backup process by using following command.
```bash
$ ./glacier-backup.sh upload --config /path/to/my_config.toml
```

### Downloading

Archive could be downloaded by sendring following command and follow the intractive promps
```bash
$ ./glacier-backup.sh retrieve archive
```

Archive retrieval takes up to 24h. After retrieval is complete, sent
```bash
$ ./glacier-backup.sh list jobs
```
to download the archive. Same command coud be used to check the status of the retrieval job.

### Retrieving inventory

In order to syncronize local database of archives, send
```bash
$ ./glacier-backup.sh retrieve inventroy
```

Inventory retrieval takes up to 24h. After retrieval is complete, sent
```bash
$ ./glacier-backup.sh list jobs
```
to download update the local database. Same command coud be used to check the status of the retrieval job.

### Delete

To delete archives send
```bash
$ ./glacier-backup.sh retrieve inventroy
```
and follow the interactive prompt.

Also it's possible to delete archive usng it's id or delete archives older than specified ammount of time
```bash
$ ./glacier-backup.sh delete archive --id VQ9hqvA...oF1SJkdhtQ
$ ./glacier-backup.sh delete archive --old 6m
```

for more options see help.

### Encryption

To configure encryption send
```bash
$ ./glacier-backup.sh encryption
```
and enter the password when prompted.